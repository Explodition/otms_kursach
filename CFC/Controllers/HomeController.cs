﻿using CFC.Models;
using CFC.Models.HomeViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace CFC.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        [HttpPost]
        public IActionResult Index(InputMathLab input)
        {
            IndexViewModel viewModel1 = new IndexViewModel();
            viewModel1.InputML = input;
            StreamWriter sw = new StreamWriter(@"serializeData.json");
            sw.Write(JsonConvert.SerializeObject(input));
            sw.Close();

            viewModel1.ResultCalc(input);

            return View(viewModel1);
        }

        public IActionResult Index()
        {
            InputMathLab input;
            IndexViewModel viewModel2 = new IndexViewModel();
            try
            {
                StreamReader sr = new StreamReader(@"serializeData.json");
                input = JsonConvert.DeserializeObject<InputMathLab>(sr.ReadToEnd());
                sr.Close();
            }
            catch
            {
                input = InputMathLab.DefaultParams();
            }
            viewModel2.InputML = input;

            return View(viewModel2);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
