﻿$("#a1").click(function () {
    $("html,body").animate({
        scrollTop: $("body").offset().top
    }, 700)
});

$("#a2").click(function () {
    $("html,body").animate({
        scrollTop: $("#result").offset().top - 100
    }, 700)
});

$("#a3").click(function () {
    $("html,body").animate({
        scrollTop: $("#myChart2").offset().top - 150
    }, 700)
});

const prov = document.querySelectorAll('.inp');
prov.forEach((item) => {
    item.addEventListener('keyup', function (event) {
        let Item = item.value;
        let ItemRep = Item;
        console.log(Item, ItemRep,Item.includes(','))
        if (Item.includes(',')) {
            ItemRep = Item.replace(',', '.');
        }
        console.log(Item, ItemRep, Item.includes(','))
        if (isNaN(+ItemRep)) {
            item.value = '';
        }
        else if (ItemRep < 0) {
            alert('Значение не может быть отрицательным!');
            item.value = '0';
        }
        else {
            if (ItemRep.includes('.')) {
                ItemRep = Item.replace('.', ',');
                item.value = ItemRep;
            }
        }
        
    })
    item.addEventListener('blur', function (event) {
        if (event.target.value == '') {
            alert('Вы ввели не все исходные данные. Проверьте, что во всех ячейках есть значения');
            item.value = '0';
        }
    })
})

let Gases = document.querySelectorAll('.gas');
let Col = document.querySelector('#summa');
Gases.forEach(item => {
    item.addEventListener('input', function (event) {
        let SumGas = 0;
        Gases.forEach(item => {
            let Item = item.value;
            let ItemRep = Item;
            if (Item.includes(',')) {
                ItemRep = Item.replace(',','.');
            }
            SumGas += +ItemRep;
        })
        if (SumGas > 100) {
            alert('Суммарный состав природного газа должен равняться 100%');
            event.target.value = '';
        }
        if (SumGas == 100) {
            Col.classList.add('summaG')
            Col.classList.remove('summaR');
        }
        else {
            Col.classList.remove('summaG')
            Col.classList.add('summaR');
        }
        document.querySelector('#summa').textContent = SumGas.toFixed(2);
    })
})

let SumGas = 0;
Gases.forEach(item => {
    let Item = item.value;
    let ItemRep = Item;
    if (Item.includes(',')) {
        ItemRep = Item.replace(',', '.');
    }
    SumGas += +ItemRep;
})
document.querySelector('#summa').textContent = SumGas.toFixed(2);