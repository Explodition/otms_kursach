﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace CFC.Models.HomeViewModels
{
    public class IndexViewModel
    {
        public InputMathLab InputML { get; set; }
        public ResultMathLab ResultML { get; set; }
        public LinearRegression Approx { get; set; }

        public IndexViewModel()
        {
            InputML = InputMathLab.DefaultParams();
        }

        public void ResultCalc(InputMathLab input)
        {
            ResultML = new ResultMathLab();
            Approx = new LinearRegression();

            // L1

            ResultML.DownBurnWorkHeat = 127.7 * input.COContent + 108 * input.H2Content + 358 * input.CH4Content + 636 * input.C2H6Content + 913 * input.C3H8Content + 1185 * input.C4H10Content + 1465 * input.C5H12Content;
            ResultML.O2Demand = 0.01 * (2 * input.CH4Content + 3.5 * input.C2H6Content + 5 * input.C3H8Content + 6.5 * input.C4H10Content + 8 * input.C5H12Content);
            ResultML.ExpectAirConsump = 4.76 * ResultML.O2Demand;
            ResultML.RealAirConsump = input.Alpha * ResultML.ExpectAirConsump;

            // L2

            ResultML.CO2BurnProdVolume = 0.01 * (input.COContent + input.CO2Content + input.CH4Content + 2 * input.C2H6Content + 3 * input.C3H8Content + 4 * input.C4H10Content + 5 * input.C5H12Content);
            ResultML.H2OBurnProdVolume = 0.01 * (input.H2Content + input.H2OContent + 2 * input.CH4Content + 3 * input.C2H6Content + 4 * input.C3H8Content + 5 * input.C4H10Content + 6 * input.C5H12Content);
            ResultML.N2BurnProdVolume = 0.01 * input.N2Content + 0.79 * ResultML.RealAirConsump;
            ResultML.O2BurnProdVolume = 0.21 * (input.Alpha - 1) * ResultML.ExpectAirConsump;
            ResultML.FullBurnProdVolume = ResultML.CO2BurnProdVolume + ResultML.H2OBurnProdVolume + ResultML.N2BurnProdVolume + ResultML.O2BurnProdVolume;
            ResultML.CO2BurnProdContent = ResultML.CO2BurnProdVolume * 100 / ResultML.FullBurnProdVolume;
            ResultML.H2OBurnProdContent = ResultML.H2OBurnProdVolume * 100 / ResultML.FullBurnProdVolume;
            ResultML.N2BurnProdContent = ResultML.N2BurnProdVolume * 100 / ResultML.FullBurnProdVolume;
            ResultML.O2BurnProdContent = ResultML.O2BurnProdVolume * 100 / ResultML.FullBurnProdVolume;

            // L3

            ResultML.AirTeploem = 0.0001 * input.AirHeatTemp + 1.2855;
            ResultML.ChemicalFuelTeplosoderzh = ResultML.DownBurnWorkHeat / ResultML.FullBurnProdVolume;
            ResultML.HeatAirTeplosoderzh = ResultML.AirTeploem * input.AirHeatTemp * ResultML.ExpectAirConsump * input.Alpha / ResultML.FullBurnProdVolume;
            ResultML.GeneralExpectBurnProdteplosoderzh = ResultML.ChemicalFuelTeplosoderzh + ResultML.HeatAirTeplosoderzh;
            ResultML.GeneralBalanceBurnProdTeplosoderzh = ResultML.GeneralExpectBurnProdteplosoderzh - 0.01 * input.Underburn * ResultML.ChemicalFuelTeplosoderzh;

            double[] Coord = new double[25];
            for (int i = 0; i < Coord.Length; i++)
            {
                Coord[i] = 1 + i;
            }

            double[] Temp = new double[25];
            for (int i = 0; i < Temp.Length; i++)
            {
                Temp[i] = 100 * (i + 1);
            }

            double[] CO2TS = new double[25];
            for (int i = 0; i < CO2TS.Length; i++)
            {
                CO2TS[i] = 2.6623 * Temp[i] - 336.76;
            }

            double[] N2TS = new double[25];
            for (int i = 0; i < N2TS.Length; i++)
            {
                N2TS[i] = 1.5354 * Temp[i] - 105.17;
            }

            double[] O2TS = new double[25];
            for (int i = 0; i < O2TS.Length; i++)
            {
                O2TS[i] = 1.6278 * Temp[i] - 114.46;
            }

            double[] H2OTS = new double[25];
            for (int i = 0; i < H2OTS.Length; i++)
            {
                H2OTS[i] = 2.0536 * Temp[i] - 246.39;
            }

            double[] FullTS = new double[25];
            for (int i = 0; i < FullTS.Length; i++)
            {
                FullTS[i] = (CO2TS[i] * ResultML.CO2BurnProdContent + N2TS[i] * ResultML.N2BurnProdContent + O2TS[i] * ResultML.O2BurnProdContent + H2OTS[i] * ResultML.H2OBurnProdContent) / 100;
            }

            var LR = new LinearRegression(Coord, FullTS);
            ResultML.LRACoef = LR.Slope;
            ResultML.LRBCoef = LR.YIntercept;

            ResultML.ExpectBurnTemp = (ResultML.GeneralExpectBurnProdteplosoderzh - LR.YIntercept) / LR.Slope * 100;
            ResultML.BalanceBurnTemp = (ResultML.GeneralBalanceBurnProdTeplosoderzh - LR.YIntercept) / LR.Slope * 100;
            ResultML.OutgoingGasTemp = input.PyroCoef * ResultML.ExpectBurnTemp;

            // L4

            ResultML.SadkaWeight = input.LengthBilletsAmount * input.WidthBilletsAmount * input.BilletWeight;
            ResultML.PodWidth = (ResultML.SadkaWeight * (input.BilletWidth + input.FreeSpaceBetweenBillets) / (input.LengthBilletsAmount * input.BilletWeight)) - input.FreeSpaceBetweenBillets + 4 * input.BilletHeight;
            ResultML.ActivePodLength = input.LengthBilletsAmount * input.BilletLength + (input.LengthBilletsAmount - 1) * 2 * input.BilletHeight;
            ResultML.FullPodLength = input.LengthBilletsAmount * input.BilletLength + (input.LengthBilletsAmount + 1) * 2 * input.BilletHeight;
            ResultML.ActivePodSquare = ResultML.ActivePodLength * (input.WidthBilletsAmount * (input.BilletHeight + input.FreeSpaceBetweenBillets) - input.FreeSpaceBetweenBillets);
            ResultML.UsePodSquare = ResultML.PodWidth * ResultML.ActivePodLength;
            ResultML.FullPodSquare = ResultML.PodWidth * ResultML.FullPodLength;

            // L5

            ResultML.PrivedenBilletRadius = input.BilletWidth * input.BilletHeight / (input.BilletWidth + input.BilletHeight);
            ResultML.KCoef = (0.8 + 1.6 * ResultML.H2OBurnProdContent / 100) * (1 - 0.00038 * (ResultML.BalanceBurnTemp + 273)) / Math.Pow((ResultML.H2OBurnProdContent + ResultML.CO2BurnProdContent) * ResultML.PrivedenBilletRadius / 100, 0.5);
            ResultML.GasBlackyDegree = 1 - 1 / Math.Exp(ResultML.KCoef / 100 * (ResultML.H2OBurnProdContent + ResultML.CO2BurnProdContent) * ResultML.PrivedenBilletRadius);
            ResultML.BilletsSquare = input.LengthBilletsAmount * input.WidthBilletsAmount * ((2 * input.BilletHeight + input.BilletWidth) * input.BilletLength + 2 * input.BilletHeight * input.BilletWidth);
            ResultML.KladkaSurfaceSquare = 2 * ResultML.FullPodLength * ResultML.PodWidth + 2 * ResultML.FullPodLength * input.WorkPlaceHeight + 2 * ResultML.PodWidth * input.WorkPlaceHeight;
            ResultML.PrivedenRadiationCoefFromGasAndKladkaToMetall = ((ResultML.KladkaSurfaceSquare / ResultML.BilletsSquare + 1 - ResultML.GasBlackyDegree) * input.MetallBlackyDegree * input.AbsolutBlackBodyIzluchCoef) / (ResultML.KladkaSurfaceSquare / ResultML.BilletsSquare + (1 - ResultML.GasBlackyDegree) / ResultML.GasBlackyDegree * (input.MetallBlackyDegree + ResultML.GasBlackyDegree * (1 - ResultML.GasBlackyDegree)));
            ResultML.AverageRayRadiationCoef = ResultML.PrivedenRadiationCoefFromGasAndKladkaToMetall * Math.Sqrt((Math.Pow((ResultML.BalanceBurnTemp + 273) / 100, 4) - Math.Pow((input.StartHeatTemp + 273) / 100, 4)) / (ResultML.BalanceBurnTemp - input.StartHeatTemp)) * Math.Sqrt((Math.Pow((ResultML.BalanceBurnTemp + 273) / 100, 4) - Math.Pow((input.FinishHeatTemp + 273) / 100, 4)) / (ResultML.BalanceBurnTemp - input.FinishHeatTemp));
            ResultML.AverageRayTeplootdachaCoef = 1.1 * ResultML.AverageRayRadiationCoef;
            ResultML.DifussionCoefOfAHeat = input.MetallTeploprovodnost / input.MetallDensity / input.UdelMetallTeploemkost;

            // L6

            ResultML.Bio = ResultML.AverageRayTeplootdachaCoef * ResultML.PrivedenBilletRadius / input.MetallTeploprovodnost;
            ResultML.NoneSurfaceTemp = (ResultML.BalanceBurnTemp - input.FinishHeatTemp) / (ResultML.BalanceBurnTemp - input.StartHeatTemp);

            if (ResultML.Bio <= 1)
            {
                ResultML.PCoef = 1 - 0.2298 * ResultML.Bio;
                ResultML.u2Coef = 1.6588 * ResultML.Bio;
                ResultML.MCoef = 1 - 0.0139 * Math.Pow(ResultML.Bio, 2) - 0.0022 * ResultML.Bio;
                ResultML.NCoef = 1 + 0.2157 * ResultML.Bio;
            }
            else
            {
                ResultML.PCoef = Math.Pow(1.173 * ResultML.Bio, -0.8363);
                ResultML.u2Coef = 0.9822 * Math.Log(ResultML.Bio) + 2.1118;
                ResultML.MCoef = 0.9606 - 0.0002 * Math.Pow(ResultML.Bio, 2) - 0.0143 * ResultML.Bio;
                ResultML.NCoef = 0.0859 * Math.Log(ResultML.Bio) + 1.3157;
            }

            ResultML.NoneHeatingTime = -Math.Log(ResultML.NoneSurfaceTemp / ResultML.PCoef) / ResultML.u2Coef;
            ResultML.HeatingTime = ResultML.NoneHeatingTime * Math.Pow(ResultML.PrivedenBilletRadius, 2) / ResultML.DifussionCoefOfAHeat;
            ResultML.NoneHeapTemp = ResultML.MCoef * Math.Exp(-ResultML.u2Coef * ResultML.NoneHeatingTime);
            ResultML.NoneCenterTemp = ResultML.NCoef * Math.Exp(-ResultML.u2Coef * ResultML.NoneHeatingTime);
            ResultML.HeapAverageTemp = ResultML.BalanceBurnTemp - (ResultML.BalanceBurnTemp - input.StartHeatTemp) * ResultML.NoneHeapTemp;
            ResultML.CenterTemp = ResultML.BalanceBurnTemp - (ResultML.BalanceBurnTemp - input.StartHeatTemp) * ResultML.NoneCenterTemp;
            ResultML.TempPerepad = Math.Abs(ResultML.CenterTemp - input.FinishHeatTemp);
        }

        // TABLE FOR GRAPHICS

        public double[] CENTER;
        public double[] KUCHA;
        public double[] TPOV;
        private double[] Perepad;
        public string JSONPerepad { get
            {
                return JsonConvert.SerializeObject(Perepad);
            } }
        public string JSONCenter
        {
            get
            {
                return JsonConvert.SerializeObject(CENTER);
            }
        }
        public string JSONKucha
        {
            get
            {
                return JsonConvert.SerializeObject(KUCHA);
            }
        }
        public string JSONTPOV
        {
            get
            {
                return JsonConvert.SerializeObject(TPOV);
            }
        }
        public string Time
        {
            get
            {
                if (ResultML != null)
                {
                    double[] _Time = new double[30];

                    double[] _SurfaceTemp = new double[30];
                    double[] _HeapTemp = new double[30];
                    double[] _CenterTemp = new double[30];
                    double[] _Perepad = new double[30];
                    for (int i = 0; i < _Time.Length; i++)
                    {
                        _Time[i] = i * 60;
                        var _F0Znach = ResultML.DifussionCoefOfAHeat * _Time[i] / Math.Pow(ResultML.PrivedenBilletRadius, 2);
                        var _NoneSurface = ResultML.PCoef * Math.Exp(-ResultML.u2Coef * _F0Znach);
                        var _NoneHeap = ResultML.MCoef * Math.Exp(-ResultML.u2Coef * _F0Znach);
                        var _NoneCenter = ResultML.NCoef * Math.Exp(-ResultML.u2Coef * _F0Znach);
                        _SurfaceTemp[i] = ResultML.BalanceBurnTemp - _NoneSurface * (ResultML.BalanceBurnTemp - InputML.StartHeatTemp);
                        _HeapTemp[i] = ResultML.BalanceBurnTemp - _NoneHeap * (ResultML.BalanceBurnTemp - InputML.StartHeatTemp);
                        _CenterTemp[i] = ResultML.BalanceBurnTemp - _NoneCenter * (ResultML.BalanceBurnTemp - InputML.StartHeatTemp);
                        _Perepad[i] = Math.Abs(_SurfaceTemp[i] - _CenterTemp[i]);
                    }
                    _Perepad[0] = 0;
                    Perepad = _Perepad;
                    TPOV = _SurfaceTemp;
                    KUCHA = _HeapTemp;
                    CENTER = _CenterTemp;
                    return JsonConvert.SerializeObject(_Time);
                }
                else
                {
                    return "";
                }
            }
        }
    }           
}

