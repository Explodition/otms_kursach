﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CFC.Models
{
    public class ResultMathLab
    {
        // L1


        public double DownBurnWorkHeat { get; set; }
        public double O2Demand { get; set; }
        public double ExpectAirConsump { get; set; }
        public double RealAirConsump { get; set; }


        // L2


        public double CO2BurnProdVolume { get; set; }
        public double H2OBurnProdVolume { get; set; }
        public double N2BurnProdVolume { get; set; }
        public double O2BurnProdVolume { get; set; }
        public double FullBurnProdVolume { get; set; }
        public double CO2BurnProdContent { get; set; }
        public double H2OBurnProdContent { get; set; }
        public double N2BurnProdContent { get; set; }
        public double O2BurnProdContent { get; set; }


        // L3


        public double AirTeploem { get; set; }
        public double ChemicalFuelTeplosoderzh { get; set; }
        public double HeatAirTeplosoderzh { get; set; }
        public double GeneralExpectBurnProdteplosoderzh { get; set; }
        public double GeneralBalanceBurnProdTeplosoderzh { get; set; }
        public double ExpectBurnTemp { get; set; }
        public double BalanceBurnTemp { get; set; }
        public double OutgoingGasTemp { get; set; }
        public double LRACoef { get; set; }
        public double LRBCoef { get; set; }



        // L4


        public double SadkaWeight { get; set; }
        public double PodWidth { get; set; }
        public double ActivePodLength { get; set; }
        public double FullPodLength { get; set; }
        public double ActivePodSquare { get; set; }
        public double UsePodSquare { get; set; }
        public double FullPodSquare { get; set; }


        // L5


        public double PrivedenBilletRadius { get; set; }
        public double GasBlackyDegree { get; set; }
        public double BilletsSquare { get; set; }
        public double KladkaSurfaceSquare { get; set; }
        public double PrivedenRadiationCoefFromGasAndKladkaToMetall { get; set; }
        public double AverageRayRadiationCoef { get; set; }
        public double AverageRayTeplootdachaCoef { get; set; }
        public double DifussionCoefOfAHeat { get; set; }
        public double AverageMetallTemp { get; set; }
        public double MetallTeploprovod { get; set; }
        public double MetallTeploemk { get; set; }
        public double KCoef { get; set; }


        // L6


        public double Bio { get; set; }
        public double NoneSurfaceTemp { get; set; }
        public double PCoef { get; set; }
        public double u2Coef { get; set; }
        public double MCoef { get; set; }
        public double NCoef { get; set; }
        public double NoneHeatingTime { get; set; }
        public double HeatingTime { get; set; }
        public double NoneHeapTemp { get; set; }
        public double NoneCenterTemp { get; set; }
        public double HeapAverageTemp { get; set; }
        public double CenterTemp { get; set; }
        public double TempPerepad { get; set; }
    }
}    
