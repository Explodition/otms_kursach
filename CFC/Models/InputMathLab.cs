﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CFC.Models
{
    public class InputMathLab
    {
        public double CH4Content { get; set; }
        public double C2H6Content { get; set; }
        public double C3H8Content { get; set; }
        public double C4H10Content { get; set; }
        public double C5H12Content { get; set; }
        public double COContent { get; set; }
        public double H2Content { get; set; }
        public double CO2Content { get; set; }
        public double N2Content { get; set; }
        public double H2OContent { get; set; }
        public double AirHeatTemp { get; set; }
        public double Alpha { get; set; }
        public double Underburn { get; set; }
        public double PyroCoef { get; set; }
        public double LengthBilletsAmount { get; set; }
        public double WidthBilletsAmount { get; set; }
        public double BilletWeight { get; set; }
        public double BilletWidth { get; set; }
        public double BilletLength { get; set; }
        public double BilletHeight { get; set; }
        public double FreeSpaceBetweenBillets { get; set; }
        public double MetallTeploprovodnost { get; set; }
        public double MetallDensity { get; set; }
        public double UdelMetallTeploemkost { get; set; }
        public double MetallBlackyDegree { get; set; }
        public double StartHeatTemp { get; set; }
        public double FinishHeatTemp { get; set; }
        public double WorkPlaceHeight { get; set; }
        public double FormCoef { get; set; }
        public double AbsolutBlackBodyIzluchCoef { get; set; }

        public static InputMathLab DefaultParams()
        {
            return new InputMathLab
            {
                CH4Content = 95.5,
                C2H6Content = 1.1,
                C3H8Content = 0.3,
                C4H10Content = 0,
                C5H12Content = 0,
                COContent = 0,
                H2Content = 0,
                CO2Content = 0.4,
                N2Content = 2.5,
                H2OContent = 0.2,
                AbsolutBlackBodyIzluchCoef = 5.7,
                MetallBlackyDegree = 0.8,
                BilletLength = 0.8,
                BilletWidth = 0.1,
                BilletHeight = 0.1,
                BilletWeight = 62.8,
                FormCoef = 2,
                LengthBilletsAmount = 1,
                WidthBilletsAmount = 10,
                FreeSpaceBetweenBillets = 0.1,
                WorkPlaceHeight = 0.5,
                MetallTeploprovodnost = 37.929,
                UdelMetallTeploemkost = 632.19,
                MetallDensity = 7859,
                Alpha = 1.05,
                AirHeatTemp = 300,
                Underburn = 2,
                PyroCoef = 0.65,
                StartHeatTemp = 20,
                FinishHeatTemp = 1200,
            };
        }
    }
}
